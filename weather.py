import requests as r
from urllib.parse import urlencode
import json 
def get_weather(lat,lon):
	if not lat or not lon:
		return json.dumps({'cod':400})
	resp=r.get('https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=4590056b542d4e630fccee5de2408068&units=metric&lang=ru'.format(lat = lat, lon = lon)).text
	return resp