from flask import Flask,request, jsonify,Response
from weather import *
app = Flask(__name__)
@app.route('/api/weather',methods=['GET'])
def hello_world():
    lat=request.args.get('lat',None)
    lon=request.args.get('lon',None)
    forecast=get_weather(lat=lat,lon=lon)
    return Response(forecast, mimetype='application/json', status=200)

app.run(host='0.0.0.0',debug = True)